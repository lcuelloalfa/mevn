# Docker MEVN (MongoDB, ExpressJS, VueJS, NodeJS) Stack

## Project setup
```
docker-compose up --build
```

### api runs on port 8080
### mongodb runs on port 27017
### ui runs on port 3000
